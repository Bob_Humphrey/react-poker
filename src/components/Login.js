import React, { useContext } from "react";
import { Auth0Context } from "../contexts/auth0-context";

function Login() {
  const { isLoading, user, loginWithRedirect, logout } = useContext(
    Auth0Context
  );
  return (
    <div className="">
      <div className="container has-text-centered">
        {!isLoading && !user && (
          <>
            <button
              onClick={loginWithRedirect}
              className="bg-red-600 text-white px-3 py-2"
            >
              Login
            </button>
          </>
        )}
        {!isLoading && user && (
          <>
            <h1>You are logged in!</h1>
            <p>Hello {user.name}</p>

            {user.picture && <img src={user.picture} alt="My Avatar" />}

            <hr />

            <button
              onClick={() => logout({ returnTo: window.location.origin })}
              className="bg-red-600 text-white px-3 py-2"
            >
              Logout
            </button>
          </>
        )}
      </div>
    </div>
  );
}

export default Login;
