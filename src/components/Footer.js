import React from "react";
import logo from "../images/bh-logo-grey.gif";

function Footer() {
  return (
    <footer className="flex w-full justify-center bg-warm-gray-200 py-4">
      <div className="w-16">
        <a href="https://bob-humphrey.com">
          <img src={logo} alt="Logo" />
        </a>
      </div>
    </footer>
  );
}

export default Footer;
